#[macro_use]
extern crate diesel;

use std::env;
use std::io;
use std::sync::Arc;

use actix_web::{cookie::Cookie, middleware, web, App, Error, HttpResponse, HttpServer};
use chrono::prelude::*;
use dotenv::dotenv;
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;
use openssl::hash::MessageDigest;
use openssl::pkey::PKey;
use openssl::sign::Signer;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;

use schema::{create_schema, Schema};
use storage::{create_storage, Storage};

mod schema;
mod storage;

#[derive(StructOpt, Debug, Clone)]
#[structopt(name = "basic")]
struct Opt {
    #[structopt(long, default_value = "0.0.0.0")]
    server_address: String,
    #[structopt(long, default_value = "8080")]
    server_port: u16,
    #[structopt(long, env = "DATABASE_URL")]
    database_url: String,
    #[structopt(long)]
    use_ssl: bool,
}

async fn graphiql(opt: web::Data<Opt>) -> HttpResponse {
    let html = graphiql_source(&format!("http://127.0.0.1:{}/graphql", opt.server_port));
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

async fn graphql(
    storage: web::Data<Arc<Box<dyn Storage + Send + Sync>>>,
    schema: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> Result<HttpResponse, Error> {
    println!("request");
    let user = web::block(move || {
        let res = data.execute(&schema, &storage);
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .await?;
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(user))
}

#[derive(Deserialize)]
struct Credentials {
    username: String,
    password: String,
}

#[derive(Serialize, Deserialize)]
enum TokenType {
    JWT,
}

#[derive(Serialize, Deserialize)]
enum JwtAlgorithm {
    HS256,
    HS384,
    HS512,
    RS256,
    RS384,
    RS512,
    ES256,
    ES384,
    ES512,
}

#[derive(Serialize, Deserialize)]
struct JwtHeader {
    alg: JwtAlgorithm,
    typ: TokenType,
}

#[derive(Serialize, Deserialize)]
enum UserRole {
    User,
    Admin,
}

#[derive(Serialize, Deserialize)]
struct TokenPayload {
    sub: String,
    name: String,
    role: UserRole,
    iat: i64,
    exp: i64,
}

async fn login(data: web::Json<Credentials>) -> Result<HttpResponse, Error> {
    let header = JwtHeader {
        alg: JwtAlgorithm::HS256,
        typ: TokenType::JWT,
    };
    let now = Utc::now();
    let key = PKey::hmac(b"my secret").unwrap();

    let payload = TokenPayload {
        sub: data.username.clone(),
        name: data.username.clone(),
        role: UserRole::Admin,
        iat: now.timestamp(),
        exp: now.timestamp(),
    };

    let config = base64::URL_SAFE_NO_PAD;

    let header_str = base64::encode_config(serde_json::to_vec(&header).unwrap(), config);
    let payload_str = base64::encode_config(serde_json::to_vec(&payload).unwrap(), config);

    let payload = format!("{}.{}", header_str, payload_str);

    let mut signer = Signer::new(MessageDigest::sha256(), &key).unwrap();
    signer.update(payload.as_bytes()).unwrap();
    let hmac = base64::encode_config(signer.sign_to_vec().unwrap(), config);

    Ok(HttpResponse::Ok()
        .cookie(Cookie::build("jwt_paylod", payload).finish())
        .cookie(Cookie::build("jwt_signature", hmac).finish())
        .body("OK"))
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
    dotenv().ok();
    let opt = Opt::from_args();

    println!("Hello, world!");

    let context = std::sync::Arc::new(create_storage(opt.database_url.clone()).unwrap());
    let schema = std::sync::Arc::new(create_schema());

    HttpServer::new({
        let opt = opt.clone();
        move || {
            App::new()
                .data(context.clone())
                .data(schema.clone())
                .data(opt.clone())
                .wrap(middleware::Logger::default())
                .service(web::resource("/login").route(web::post().to(login)))
                .service(web::resource("/graphql").route(web::post().to(graphql)))
                .service(web::resource("/graphiql").route(web::get().to(graphiql)))
        }
    })
    .bind(format!("{}:{}", opt.server_address, opt.server_port))?
    .run()
    .await
}
