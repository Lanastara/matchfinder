use juniper::FieldResult;
use juniper::RootNode;

use juniper::{GraphQLObject, GraphQLEnum, GraphQLInputObject};

use super::storage::Storage;
use uuid::Uuid;
use chrono::{Utc, DateTime};

pub trait VecInto<TOut> {
    fn vec_into(self) -> Vec<TOut>;
}

impl<TIn, TOut> VecInto<TOut> for Vec<TIn>
where TIn : Into<TOut>
{
    fn vec_into(mut self) -> Vec<TOut>{
        self.drain(..).map(|input| input.into()).collect()
    }
}

#[derive(GraphQLEnum)]
pub enum FilterField {
    Title,
    Tags,
}

#[derive(GraphQLInputObject)]
pub struct Filter {
    field: FilterField,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "A tag that can be used for filtering posts")]
struct NewTag{
    name: String,
    description: Option::<String>
}

impl Into<super::storage::Tag> for NewTag
{
    fn into(self) -> super::storage::Tag{
        super::storage::Tag{
            name: self.name,
            description: self.description
        }
    }
}

#[derive(GraphQLObject)]
#[graphql(description = "A tag that can be used for filtering posts")]
struct Tag{
    name: String,
    description: Option::<String>
}

impl From<super::storage::Tag> for Tag
{
    fn from(tag: super::storage::Tag) -> Tag{
        Tag{
            name: tag.name,
            description: tag.description
        }
    }
}

#[derive(GraphQLInputObject)]
pub struct NewPost {
    title: String,
    tags: Vec<NewTag>,
    text: String,
}

impl Into<super::storage::Post> for NewPost
{
    fn into(self) -> super::storage::Post{
        super::storage::Post{
            id: Uuid::new_v4(),
            owner: super::storage::Owner::OneTime("".to_string()),
            title: self.title,
            tags: self.tags.vec_into(),
            text: self.text,
            creation: Utc::now()
        }
    }
}

#[derive(GraphQLObject)]
pub struct Post {
    id: Uuid,
    owner: String,
    title: String,
    tags: Vec<Tag>,
    text: String,
    creation: DateTime<Utc>,
}

impl From<super::storage::Post> for Post
{
    fn from(post: super::storage::Post) -> Post{
        Post{
            id: post.id,
            owner: post.owner.into(),
            title: post.title,
            tags: post.tags.vec_into(),
            text: post.text,
            creation: post.creation,
        }
    }
}

pub struct QueryRoot;

#[juniper::object(
    Context = Box<dyn Storage + Send + Sync>
)]
impl QueryRoot {
    fn tags(context: &Box<dyn Storage + Send + Sync>) -> Vec<Tag> {
        context.get_tags().vec_into()
    }

    fn posts(filter: Vec<Filter>, context: &Box<dyn Storage + Send + Sync>) -> FieldResult<Vec<Post>> {
        Ok(context.get_posts()?.vec_into())
    }
}

pub struct MutationRoot;

#[juniper::object(
    Context = Box<dyn Storage + Send + Sync>
)]
impl MutationRoot {
    fn add_tag(tag: NewTag, context: &Box<dyn Storage + Send + Sync>) -> FieldResult<Tag>
    {
        Ok(context.add_tag(tag.into())?.into())
    }

    fn create_post(post: NewPost, context: &Box<dyn Storage + Send + Sync>) -> FieldResult<Post>
    {
        Ok(context.add_post(post.into())?.into())
    }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}