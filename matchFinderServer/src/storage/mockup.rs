use super::{Tag, Storage};
use std::sync::Mutex;

#[derive(Debug)]
pub struct MockupStorage
{
    tags: Mutex<Vec<Tag>>
}

impl MockupStorage{
    pub fn new() -> Self
    {
        MockupStorage
        {
            tags: Mutex::new(vec![Tag{
                name: "Test".to_string(),
                description: None
            }])
        }
    }
}

impl Storage for MockupStorage{
    fn get_tags(&self) -> Vec<Tag> {
        self.tags.lock().unwrap().clone()
    }

    fn add_tag(&self, tag: Tag) -> anyhow::Result<Tag> {

        todo!()
    }
    fn get_posts(&self) -> anyhow::Result<Vec<super::Post>> {
        todo!()
    }
    fn add_post(&self, post: super::Post) -> anyhow::Result<super::Post> {
        todo!()
    }
}