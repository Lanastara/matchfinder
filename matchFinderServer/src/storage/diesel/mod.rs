use diesel::prelude::*;
use diesel::insert_into;
use super::Storage;

pub mod schema;
mod models;

use models::*;
use std::convert::TryInto;

type Connection = SqliteConnection;

pub fn create_storage<S: Into<String>>(connection_string :S) -> Result<Box<dyn Storage + Send + Sync>, anyhow::Error>
{
    let manager = diesel::r2d2::ConnectionManager::<Connection>::new(connection_string);
    let pool = diesel::r2d2::Builder::new()
        .max_size(15)
        .build(manager)
        ?;


    Ok(Box::new(DieselStorage{pool}))
}

struct DieselStorage
{
    pool: diesel::r2d2::Pool<diesel::r2d2::ConnectionManager<Connection>>
}

impl Storage for DieselStorage
{
    fn get_tags(&self) -> Vec<super::Tag> {
        use schema::tags::dsl::*;
        let connection = self.pool.get().unwrap();
        let mut result = tags.order(name.asc()).load::<Tag>(&connection).unwrap();
        let result = result.drain(..).map::<super::Tag, _>(Tag::into);
        result.collect()
    }

    fn add_tag(&self, tag: super::Tag) -> Result<super::Tag, anyhow::Error> {
        use schema::tags::dsl::*;
        let connection = self.pool.get()?;

        insert_into(tags).values(Tag::from(tag.clone())).execute(&connection)?;
        Ok(tag)
    }

    fn get_posts(&self) -> anyhow::Result<Vec<super::Post>> {
        use schema::posts::dsl::*;
        let connection = self.pool.get().unwrap();
        let mut result = posts.order(creation.desc()).load::<Post>(&connection).unwrap();
        let result = result.drain(..).map::<Result<super::Post, anyhow::Error>, _>(Post::try_into);
        result.collect()
    }

    fn add_post(&self, post: super::Post) -> Result<super::Post, anyhow::Error> {
        use schema::posts::dsl::*;
        let connection = self.pool.get()?;

        insert_into(posts).values(Post::from(post.clone())).execute(&connection)?;
        Ok(post)
    }
}