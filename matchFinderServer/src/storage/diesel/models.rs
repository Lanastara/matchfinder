use super::schema::*;
use uuid::Uuid;
use chrono::prelude::*;
use std::convert::TryInto;

#[derive(Queryable, Insertable)]
#[table_name = "tags"]
pub struct Tag {
    pub name: String,
    pub description: Option<String>,
}

impl Into<super::super::Tag> for Tag{
    fn into(self) -> super::super::Tag{
        super::super::Tag{
            name: self.name,
            description: self.description
        }
    }
}

impl From<super::super::Tag> for Tag{
    fn from(tag: super::super::Tag) -> Self {
        Self{
            name: tag.name,
            description: tag.description
        }
    }
}

#[derive(Queryable, Insertable)]
#[table_name = "posts"]
pub struct Post {
    id: String,
    owner: String,
    title: String,
    text: String,
    creation: i64,
}

impl TryInto<super::super::Post> for Post{
    type Error = anyhow::Error;

    fn try_into(self) -> Result<super::super::Post, anyhow::Error>{
        Ok(super::super::Post{
            id: Uuid::parse_str(&self.id)?,
            owner: super::super::Owner::OneTime(self.owner),
            title: self.title,
            text: self.text,
            creation: Utc.timestamp( self.creation, 0),
            tags: Vec::new(),
        })
    }
}

impl From<super::super::Post> for Post{
    fn from(tag: super::super::Post) -> Self {
        Self{
            id: tag.id.to_string(),
            owner: tag.owner.into(),
            title: tag.title,
            text: tag.text,
            creation: tag.creation.timestamp(),
        }
    }
}