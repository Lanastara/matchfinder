table! {
    messages (id) {
        id -> Text,
        title -> Text,
        text -> Text,
        post -> Text,
    }
}

table! {
    posts (id) {
        id -> Text,
        owner -> Text,
        title -> Text,
        text -> Text,
        creation -> BigInt,
    }
}

table! {
    posts_tags (post, tag) {
        post -> Text,
        tag -> Text,
    }
}

table! {
    tags (name) {
        name -> Text,
        description -> Nullable<Text>,
    }
}

table! {
    users (id) {
        id -> Text,
        name -> Text,
        password_hash -> Text,
    }
}

joinable!(messages -> posts (post));
joinable!(posts_tags -> posts (post));
joinable!(posts_tags -> tags (tag));

allow_tables_to_appear_in_same_query!(
    messages,
    posts,
    posts_tags,
    tags,
    users,
);
