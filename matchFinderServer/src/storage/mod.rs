pub mod mockup;
pub mod diesel;

use chrono::prelude::*;
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct User {
    id: Uuid,
    name: String,
    password_hash: String
}

#[derive(Debug, Clone, PartialEq)]
pub struct Tag {
    pub name: String,
    pub description: Option<String>
}

#[derive(Debug, Clone)]
pub enum Owner{
    User(User),
    OneTime(String),
}

impl Into<String> for Owner {
    fn into(self) -> String {
        match self {
            Owner::User(user) => user.name,
            Owner::OneTime(one_time) => one_time
        }
    }
}

#[derive(Debug, Clone)]
pub struct Post {
    pub id: Uuid,
    pub owner: Owner,
    pub title: String,
    pub tags: Vec<Tag>,
    pub text: String,
    pub creation: DateTime<Utc>,
}

pub struct Message {
    id: Uuid,
    text: String,
}

pub trait Storage
{
    fn get_tags(&self) -> Vec<Tag>;

    fn get_posts(&self) -> anyhow::Result<Vec<Post>>;

    fn add_tag(&self, tag: Tag) -> Result<Tag, anyhow::Error>;

    fn add_post(&self, post: Post) -> Result<Post, anyhow::Error>;
}

pub fn create_storage<S: Into<String>>(connection_string :S) -> Result<Box<dyn Storage + Send + Sync>, anyhow::Error>
{
    diesel::create_storage(connection_string)

    //Ok(Box::new(mockup::MockupStorage::new()))
}