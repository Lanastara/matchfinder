-- This file should undo anything in `up.sql`

DROP TABLE "messages";
DROP TABLE "posts_tags";
DROP TABLE "posts";
DROP TABLE "users";
DROP TABLE "tags";