-- Your SQL goes here
PRAGMA foreign_keys = ON;

CREATE TABLE tags (
    "name" VARCHAR PRIMARY KEY NOT NULL,
    "description" VARCHAR
);

CREATE TABLE users (
    "id" VARCHAR PRIMARY KEY NOT NULL,
    "name" VARCHAR NOT NULL,
    "password_hash" VARCHAR NOT NULL
);

CREATE TABLE posts (
    "id" VARCHAR PRIMARY KEY NOT NULL,
    "owner" VARCHAR NOT NULL,
    "title" VARCHAR NOT NULL,
    "text" VARCHAR NOT NULL,
    "creation" BIGINT NOT NULL
);

CREATE TABLE posts_tags (
    "post" VARCHAR NOT NULL,
    "tag" VARCHAR NOT NULL,
    PRIMARY KEY ("post", "tag"),
    FOREIGN KEY("post") REFERENCES posts("id"),
    FOREIGN KEY("tag") REFERENCES tags("name")
);

CREATE TABLE messages (
    "id" VARCHAR PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "text" VARCHAR NOT NULL,
    "post" VARCHAR NOT NULL,
    FOREIGN KEY("post") REFERENCES posts("id")
);
